for n in {1 2 4 8 16 32}; do
	echo using $n processors
	mpirun -np $n -machinefile mpi.machines benchmark
done
