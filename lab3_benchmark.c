#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <mpi.h>

#define VECSIZE 5000
#define ITERATIONS 1000
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
float max(float num1, float num2){
	if (num1 > num2){
		return num1;
	} else {
		return num2;
	}	
}


float BcastVec(int numdim, int rank, int size, float *vector)
{
	int notparticipating = pow(2, numdim-1)-1;
	int bitmask = pow(2,numdim-1);
	int curdim, i;
	float *newvector = vector;
	
	MPI_Status status;
	
	for (curdim = 0; curdim < numdim; curdim++){
		if ((rank & notparticipating) == 0){
			if ((rank & bitmask) == 0){
				int msg_dest = rank ^ bitmask;
				MPI_Send(vector, size, MPI_FLOAT, msg_dest, 2, MPI_COMM_WORLD);
			} else {
			    int msg_src = rank ^ bitmask;
				MPI_Recv(newvector, size, MPI_FLOAT, msg_src, 2, MPI_COMM_WORLD, &status);
				
			}
		}
		notparticipating >>= 1;
		bitmask >>= 1;	
	}
	vector = newvector;
}

float RedMaxVec(int numdim, int rank, int size, float *vector, float *maxVector)
{
	int notparticipating = 0;
	int bitmask = 1;
	int curdim, i;
	float newMaxVector[VECSIZE];
	MPI_Status status;
	
	for (i=0; i<size; i++){
		maxVector[i] = vector[i];
		newMaxVector[i] = vector[i];
	}	
	
	for (curdim = 0; curdim < numdim; curdim++) {
	    if ((rank & notparticipating) == 0) {
    	    if ((rank & bitmask) != 0) {
				//printf("sender: %i\n", rank);
	            int msg_dest = rank ^ bitmask;
				//printf("msg_dest %i\n", msg_dest);
	            MPI_Send(maxVector, size, MPI_FLOAT, msg_dest, 1, MPI_COMM_WORLD);
			} else {
				//printf("receiver: %i\n", rank);
	            int msg_src = rank ^ bitmask;
				//printf("msg_src %i\n", msg_src);
	            MPI_Recv(&newMaxVector, size, MPI_FLOAT, msg_src, 1, MPI_COMM_WORLD, &status);
	            for (i=0; i<size; i++){
					//printf("old: %f, new: %f\n", maxVector[i], newMaxVector[i]);
					maxVector[i] = max(maxVector[i], newMaxVector[i]);
				}
	        }
        }
        notparticipating = notparticipating ^ bitmask;
        bitmask <<=1;
	}
}


int main(int argc, char *argv[])
{
        int iproc, nproc,i, iter, numdims;
        char host[255], message[55];
        MPI_Status status;

        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
		numdims = log2(nproc);
        gethostname(host,253);
        printf("I am proc %d of %d running on %s\n", iproc, nproc,host);
        // each process has an array of VECSIZE double: ain[VECSIZE]
        float *ain, *aout;
		ain = (float *)malloc(VECSIZE*sizeof(float));
		aout = (float *)malloc(VECSIZE*sizeof(float));
        int  ind[VECSIZE];
        struct {
            float val;
            int   rank;
        } in[VECSIZE], out[VECSIZE];
        int myrank, root = 0;

        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
        // Start time here
        srand(myrank+5);
        double start = When();
        for(iter = 0; iter < ITERATIONS; iter++) {
			for(i = 0; i < VECSIZE; i++) {
				ain[i] = rand();
	//          printf("init proc %d [%d]=%f\n",myrank,i,ain[i]);
			}
			for (i=0; i<VECSIZE; ++i) {
				in[i].val = ain[i];
				in[i].rank = myrank;
			}
			RedMaxVec(numdims, myrank, VECSIZE, ain, aout);
			// At this point, the answer resides on process root
			//if (myrank == root) {
				/* read ranks out
				 */
			//	for (i=0; i<VECSIZE; ++i) {
	//              printf("root out[%d] = %f from %d\n",i,out[i].val,out[i].rank);
			//		aout[i] = out[i].val;
			//		ind[i] = out[i].rank;
			//	}
			//}
			
				
			// Now broadcast this max vector to everyone else.
			BcastVec(numdims, myrank, VECSIZE, aout);
			for(i = 0; i < VECSIZE; i++) {
	//        printf("final proc %d [%d]=%f from %d\n",myrank,i,out[i].val,out[i].rank);
			}
        }
        MPI_Finalize();
        double end = When();
        if(myrank == root) {
          printf("Time %f\n",end-start);
        }
		
		return 0;
}
