#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#define	MASTER 0
#define SIZE 1

float max(float num1, float num2){
	if (num1 > num2){
		return num1;
	} else {
		return num2;
	}	
}


float BcastVec(int numdim, int rank, int size, float *vector)
{
	int notparticipating = pow(2, numdim-1)-1;
	int bitmask = pow(2,numdim-1);
	int curdim, i;
	float *newvector = vector;
	
	MPI_Status status;
	
	for (curdim = 0; curdim < numdim; curdim++){
		if ((rank & notparticipating) == 0){
			if ((rank & bitmask) == 0){
				int msg_dest = rank ^ bitmask;
				MPI_Send(vector, size, MPI_FLOAT, msg_dest, 2, MPI_COMM_WORLD);
			} else {
			    int msg_src = rank ^ bitmask;
				MPI_Recv(newvector, size, MPI_FLOAT, msg_src, 2, MPI_COMM_WORLD, &status);
				
			}
		}
		notparticipating >>= 1;
		bitmask >>= 1;	
	}
	vector = newvector;
}

float RedMaxVec(int numdim, int rank, int size, float *vector, float *maxVector, int *ranks)
{
	int notparticipating = 0;
	int bitmask = 1;
	int curdim, i;
	float newMaxVector[SIZE];
	MPI_Status status;
	
	for (i=0; i<size; i++){
		maxVector[i] = vector[i];
		newMaxVector[i] = vector[i];
	}	
	
	for (curdim = 0; curdim < numdim; curdim++) {
	    if ((rank & notparticipating) == 0) {
    	    if ((rank & bitmask) != 0) {
				//printf("sender: %i\n", rank);
	            int msg_dest = rank ^ bitmask;
				//printf("msg_dest %i\n", msg_dest);
	            MPI_Send(maxVector, size, MPI_FLOAT, msg_dest, 1, MPI_COMM_WORLD);
			} else {
				//printf("receiver: %i\n", rank);
	            int msg_src = rank ^ bitmask;
				//printf("msg_src %i\n", msg_src);
	            MPI_Recv(&newMaxVector, size, MPI_FLOAT, msg_src, 1, MPI_COMM_WORLD, &status);
	            for (i=0; i<size; i++){
					//printf("old: %f, new: %f\n", maxVector[i], newMaxVector[i]);
					maxVector[i] = max(maxVector[i], newMaxVector[i]);
				}
	        }
        }
        notparticipating = notparticipating ^ bitmask;
        bitmask <<=1;
	}
}


int main(int argc, char *argv[])
{
	int i, numtasks, taskid, len, send, recv, numdims, size;
	float startVal = 1.0, value, *vector, *maxVector;
	int *ranks;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	
	vector = (float *)malloc(SIZE*sizeof(float));
	
	maxVector = (float *)malloc(SIZE*sizeof(float));
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
	MPI_Get_processor_name(hostname, &len);
	
	numdims = log2(numtasks);
	
    
	if ((numtasks & ~(numtasks-1))!=numtasks){
	    if (taskid == MASTER){ 
	        printf("ERROR: Please use a number of tasks that is a power of two. numtasks = %d\n", numtasks);
	    }	    
	} else {
		if (taskid == MASTER){
            printf("MASTER=%d: Number of MPI tasks is %d\n", taskid, numtasks);	
		}
		
		size = SIZE;
		srand(2.*taskid);
		
		for (i=0; i<size; i++){
			vector[i] = (float)rand()/(float)RAND_MAX;
		}
		
		for (i=0; i<numtasks; i++){
			if (i == taskid){
				printf("on task %i: values in array: \n", taskid);			
				for (int i = 0; i<size; i ++) {				
					printf(" %f \n", vector[i]);		
				}
			}
			MPI_Barrier(MPI_COMM_WORLD);
		}
				
        RedMaxVec(numdims, taskid, size, vector, maxVector, ranks);
    
		if (taskid == MASTER){
            printf("original max vector: \n");
			for (i=0; i<size; i++){
				printf("%f \n", maxVector[i]);
			}
		}
	
        BcastVec(numdims, taskid, size, maxVector);	
    
        //printf("from task %d, value = %f\n", taskid, value);
		
        if (taskid == MASTER){
            printf("max vector: \n");
			for (i=0; i<size; i++){
				printf("%f \n", maxVector[i]);
			}
		}
		
		for (i=0; i<numtasks; i++){
			if (i == taskid){
				printf("on task %i: max vector: \n", taskid);			
				for (int i = 0; i<size; i ++) {				
					printf(" %f \n", maxVector[i]);				
				}
			}
			MPI_Barrier(MPI_COMM_WORLD);
		}
    } 
    
	free(vector);
	free(maxVector);
	free(ranks);
	MPI_Finalize();	
	
	
}

