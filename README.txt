This source is to be run on the CS open lab computers at BYU.

before compiling, make MPI available on your path:
$ export PATH=/users/faculty/snell/mpich/bin:$PATH

compile using
$ mpicc -lm -O3 -std=gnu99 VectorRedMaxBcast.c -o VectorRedMaxBcast

run using
$ mpirun -np <n> -machinefile mpi.machinefile VectorRedMaxBcast
